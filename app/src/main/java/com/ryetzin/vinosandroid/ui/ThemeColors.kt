package com.ryetzin.vinosandroid.ui

import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.ui.graphics.Color

private val Yellow200 = Color(0xffffeb46)
private val Blue200 = Color(0xff91a4fc)
private val Yellow500 = Color(0xFFFF9800)
private val Yellow400 = Color(0xFFFFC107)
private val Blue700 = Color(0xFF3F51B5)
private val Brown                   = Color(0xFF503629)
private val BrownBackgroundFooter   = Color(0xffddd8cb)
private val BlackMate               = Color(0xFF1f1f1f)
private val Red                     = Color(0xFFe62236)
private val GrayInitial             = Color(0xFFf0f0f0)
private val GrayFinal               = Color(0xFFdfdfdf)
private val GrayBackgroundItem      = Color(0xffefefef)

val DarkColorsTheme = darkColors(
    primary = Yellow200,
    secondary = Blue200,
)
val LightColorsTheme = lightColors(
    primary = Yellow500,
    primaryVariant = Yellow400,
    secondary = Blue700,
)